import React, { memo } from "react";
import Grid from "@material-ui/core/Grid";
import FacebookIcon from "@material-ui/icons/Facebook";
import TextField from "@material-ui/core/TextField";
import AccountCircle from "@material-ui/icons/AccountCircle";
import SearchIcon from "@material-ui/icons/Search";
function Facebook(props) {
  return (
    <Grid container spacing={3} justify="Space-between">
      <Grid item xs={12}>
        <Grid>
          <FacebookIcon />
        </Grid>
        <Grid container spacing={1}>
          <Grid item>
            <SearchIcon />
          </Grid>
          <Grid item>
            <TextField id="input-with-icon-grid" label="Search Facebook" />
          </Grid>
        </Grid>
      </Grid>
    </Grid>
  );
}
export default memo(Facebook);
