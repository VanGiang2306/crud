import React, { useState } from "react";
import CssBaseline from "@material-ui/core/CssBaseline";
import Typography from "@material-ui/core/Typography";
import Container from "@material-ui/core/Container";
import Input from "@material-ui/core/Input";

export default function SimpleContainer() {
  const [data, setData] = useState([
    {
      id: 1,
      address: "HN",
      gender: 0,
      phone: "0988773243",
      hob: ["TV", "GAME"],
      dob: 2001,
      age: 19,
      username: "Phuong",
    },
    {
      id: 2,
      address: "HP",
      gender: 1,
      phone: "123",
      hob: ["FOOD", "READ"],
      dob: 1997,
      age: 24,
      username: "Giang",
    },
    {
      id: 3,
      address: "GN",
      gender: 0,
      phone: "123",
      hob: ["TV", "GAME"],
      dob: 1997,
      age: 25,
      username: "Hung",
    },
  ]);
  console.log(data);
  return (
    <React.Fragment>
      <CssBaseline />
      <Container
        maxWidth="sm"
        maxHeight="sm"
        style={{
          margin: "40px auto",
          backgroundColor: "#cfe8fc",
          height: "auto",
          padding: "40px",
        }}
      >
        <Typography variant="h2" gutterBottom style={{ textAlign: "center" }}>
          Đăng nhập
        </Typography>
        <Input
          type="text"
          name="text"
          // onChange={handleChangeInput}
          placeholder="Input"
          fullWidth={true}
          style={{ height: "40px", background: "#ffffff", padding: "0 15px" }}
        />
      </Container>
    </React.Fragment>
  );
}
