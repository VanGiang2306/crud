import React, { useState, useEffect } from "react";
import CssBaseline from "@material-ui/core/CssBaseline";
import Typography from "@material-ui/core/Typography";
import Container from "@material-ui/core/Container";
import Input from "@material-ui/core/Input";
import Checkbox from "@material-ui/core/Checkbox";
import Select from "@material-ui/core/Select";
import InputLabel from "@material-ui/core/InputLabel";
import MenuItem from "@material-ui/core/MenuItem";
import FormControl from "@material-ui/core/FormControl";
import Button from "@material-ui/core/Button";
import { Grid } from "@material-ui/core";
export default function List() {
  const [data, setData] = useState([
    {
      id: 1,
      name: "Giang",
      Phone: "123",
      address: "HN",
      hob: ["Game", "tv"],
      dob: 1997,
      gender: "1",
    },
    {
      id: 2,
      name: "Hung",
      Phone: "456",
      address: "SG",
      hob: ["Girl", "Listen"],
      dob: 1996,
      gender: "0",
    },
    {
      id: 1,
      name: "Phuong",
      Phone: "789",
      address: "DN",
      hob: ["Sleep", "Eat"],
      dob: 2001,
      gender: "0",
    },
  ]);
  const [count, setCount] = useState(0);
  console.log(count);
  const [countBoy, setCountBoy] = useState(0);
  const [countGirl, setCountGirl] = useState(0);
  const [max, setMax] = useState(0);
  const handleChange = (e) => {
    const value = e.target.value;
    const name = e.target.name;
    setData({ ...data, [name]: value });
  };
  const handleClick = (e) => {
    e.preventDefault();
    console.log(1, data);
  };

  useEffect(() => {
    console.log(data);
    setCount(data.length);
    let boy = 0;
    let girl = 0;
    data.map((element) => {
      if (element.gender === "1") {
        girl = girl + 1;
      } else if (element.gender === "0") {
        boy = boy + 1;
      }
    });
    let max = data.reduce(function (a, b) {
      return { dob: Math.min(a.dob, b.dob) };
    });
    console.log("max", max.dob);
    console.log("boy", boy);
    console.log("girl", girl);
    setMax(max.dob);
    setCountBoy(boy);
    setCountGirl(girl);
  }, []);

  return (
    <React.Fragment>
      <CssBaseline />
      <Grid container spacing={8}>
        Giang xau xi
      </Grid>
      <Grid container={true}>Giang xau xi</Grid>
      <Container
        maxWidth="md"
        maxHeight="md"
        style={{
          margin: "40px auto",
          backgroundColor: "#cfe8fc",
          height: "100vh",
        }}
      >
        <Typography
          variant="h2"
          gutterBottom
          style={{ textAlign: "center", marginTop: "50px" }}
        >
          đăng nhập
        </Typography>
        <Input
          type="text"
          name="name"
          MuiInput-fullWidth
          style={{ height: "40px", background: "white", marginTop: "20px" }}
          fullWidth={true}
          onChange={handleChange}
          value={data.name}
        />
        <Button
          variant="contained"
          color="primary"
          href="#contained-buttons"
          style={{ display: "block", textAlign: "center", marginTop: "50px" }}
          onClick={handleClick}
        >
          Login
        </Button>
        <div>
          {data.map((element) => {
            return (
              <p key={element.id}>
                {element.gender === 0
                  ? "nam"
                  : element.gender === 1
                  ? "nu"
                  : "khac"}
                - {element.hob.join(",")}
              </p>
            );
          })}
        </div>
        <Typography>nguoi lon tuoi nhat :{max}</Typography>
        <Typography>Boy: {countBoy}</Typography>
        <Typography>Girl: {countGirl}</Typography>
        <Typography> {count}</Typography>
      </Container>
    </React.Fragment>
  );
}
