import React, { useState } from "react";
import CssBaseline from "@material-ui/core/CssBaseline";
import Typography from "@material-ui/core/Typography";
import Container from "@material-ui/core/Container";
import Input from "@material-ui/core/Input";
import Checkbox from "@material-ui/core/Checkbox";
import Select from "@material-ui/core/Select";
import InputLabel from "@material-ui/core/InputLabel";
import MenuItem from "@material-ui/core/MenuItem";
import FormControl from "@material-ui/core/FormControl";
import Button from "@material-ui/core/Button";
import FormControlLabel from "@material-ui/core/FormControlLabel";

export default function SimpleContainer() {
  const [data, setData] = useState({
    username: "",
    password: "",
    remember: true,
    select: 2,
  });
  const handleChange = (e) => {
    const value = e.target.value;
    const name = e.target.name;
    setData({ ...data, [name]: value });
  };

  const handleChecked = (e) => {
    const value = e.target.checked;
    const name = e.target.name;
    setData({ ...data, [name]: value });
  };
  const handleSelect = (e) => {
    const value = e.target.value;
    // const name = e.target.name;
    setData({ ...data, select: value });
  };
  const handleClick = (e) => {
    e.preventDefault();
    console.log(1, data);
  };
  console.log(data);
  return (
    <React.Fragment>
      <CssBaseline />
      <Container
        maxWidth="md"
        maxHeight="md"
        style={{
          margin: "40px auto",
          backgroundColor: "#cfe8fc",
          height: "100vh",
        }}
      >
        <Typography
          variant="h2"
          gutterBottom
          style={{ textAlign: "center", marginTop: "50px" }}
        >
          đăng nhập
        </Typography>
        <Input
          type="text"
          name="username"
          MuiInput-fullWidth
          style={{ height: "40px", background: "white", marginTop: "20px" }}
          fullWidth={true}
          onChange={handleChange}
          value={data.username}
        />
        <br />
        <Input
          type="text"
          name="password"
          MuiInput-fullWidth
          style={{ height: "40px", background: "white", marginTop: "20px" }}
          fullWidth={true}
          onChange={handleChange}
          value={data.password}
        />
        <Checkbox
          defaultChecked
          color="default"
          inputProps={{ "aria-label": "checkbox with default color" }}
          onChange={handleChecked}
          checked={data.remember}
          name="remember"
        ></Checkbox>
        <FormControl style={{ float: "right", margin: "20px 0" }}>
          <InputLabel id="demo-simple-select-label">Age</InputLabel>
          <Select
            onChange={handleSelect}
            value={data.select}
            labelId="demo-simple-select-label"
            id="demo-simple-select"
            style={{ width: "100px" }}
          >
            <MenuItem value="1">Ten</MenuItem>
            <MenuItem value="2">Twenty</MenuItem>
            <MenuItem value="3">Thirty</MenuItem>
          </Select>
        </FormControl>

        <Button
          variant="contained"
          color="primary"
          href="#contained-buttons"
          style={{ display: "block", textAlign: "center", marginTop: "50px" }}
          onClick={handleClick}
        >
          Login
        </Button>
      </Container>
    </React.Fragment>
  );
}
