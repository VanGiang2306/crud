import React, { memo } from "react";
import Grid from "@material-ui/core/Grid";
import TextField from "@material-ui/core/TextField";
import "./../App.css";
function Face(props) {
  return (
    <Grid container direction="row" justify="flex-end" alignItems="flex-end">
      <Grid
        spacing={4}
        container
        direction="row"
        justify="flex-end"
        alignItems="flex-end"
        wrap="nowrap"
      >
        <Grid item>
          <TextField
            id="datetime-local"
            label="Next appointment"
            type="datetime-local"
            defaultValue="2017-05-24T10:30"
            InputLabelProps={{
              shrink: true,
            }}
          />
        </Grid>
        <Grid item>
          <TextField
            id="datetime-local"
            label="Next appointment"
            type="datetime-local"
            defaultValue="2017-05-24T10:30"
            InputLabelProps={{
              shrink: true,
            }}
          />
        </Grid>

        <Grid item>
          <TextField
            id="datetime-local"
            label="Next appointment"
            type="datetime-local"
            defaultValue="2017-05-24T10:30"
            InputLabelProps={{
              shrink: true,
            }}
          />
        </Grid>
        <Grid item>
          <TextField
            id="datetime-local"
            label="Next appointment"
            type="datetime-local"
            defaultValue="2017-05-24T10:30"
            InputLabelProps={{
              shrink: true,
            }}
          />
        </Grid>
        <Grid item>
          <TextField
            id="datetime-local"
            label="Next appointment"
            type="datetime-local"
            defaultValue="2017-05-24T10:30"
            InputLabelProps={{
              shrink: true,
            }}
          />
        </Grid>
        <Grid item>
          <TextField
            label="With normal TextField"
            id="standard-start-adornment"
          />
        </Grid>
        <Grid item>
          <TextField
            label="With normal TextField"
            id="standard-start-adornment"
          />
        </Grid>
      </Grid>
      <Grid spacing={4} container direction="row">
        <Grid item>
          <TextField
            id="datetime-local"
            label="Next appointment"
            type="datetime-local"
            defaultValue="2017-05-24T10:30"
            InputLabelProps={{
              shrink: true,
            }}
          />
        </Grid>
        <Grid item>
          <TextField
            label="With normal TextField"
            id="standard-start-adornment"
          />
        </Grid>
      </Grid>
      <Grid
        spacing={4}
        container
        direction="row"
        justify="flex-end"
        alignItems="flex-end"
      >
        <Grid item xs={4}>
          Nếu bạn muốn trở thành một lập trình viên front end, ví dụ, dự án kiểu
          timeline bằng Javascript có thể là một dự án / tính năng rất hay được
          khách hàng yêu cầu xây dựng. Các doanh nghiệp và start-up thường muốn
          hiển thị các cột mốc quan trọng nhất của họ trên trang web.
        </Grid>
        <Grid item xs={4}>
          Nếu bạn muốn trở thành một lập trình viên front end, ví dụ, dự án kiểu
          timeline bằng Javascript có thể là một dự án / tính năng rất hay được
          khách hàng yêu cầu xây dựng. Các doanh nghiệp và start-up thường muốn
          hiển thị các cột mốc quan trọng nhất của họ trên trang web.
        </Grid>
        <Grid item xs={4}>
          Nếu bạn muốn trở thành một lập trình viên front end, ví dụ, dự án kiểu
          timeline bằng Javascript có thể là một dự án / tính năng rất hay được
          khách hàng yêu cầu xây dựng. Các doanh nghiệp và start-up thường muốn
          hiển thị các cột mốc quan trọng nhất của họ trên trang web.
        </Grid>
        <Grid item xs={4}>
          Nếu bạn muốn trở thành một lập trình viên front end, ví dụ, dự án kiểu
          timeline bằng Javascript có thể là một dự án / tính năng rất hay được
          khách hàng yêu cầu xây dựng. Các doanh nghiệp và start-up thường muốn
          hiển thị các cột mốc quan trọng nhất của họ trên trang web.
        </Grid>
        <Grid item xs={4}>
          Nếu bạn muốn trở thành một lập trình viên front end, ví dụ, dự án kiểu
          timeline bằng Javascript có thể là một dự án / tính năng rất hay được
          khách hàng yêu cầu xây dựng. Các doanh nghiệp và start-up thường muốn
          hiển thị các cột mốc quan trọng nhất của họ trên trang web.
        </Grid>
        {/* <Grid item xs={4}>
          Nếu bạn muốn trở thành một lập trình viên front end, ví dụ, dự án kiểu
          timeline bằng Javascript có thể là một dự án / tính năng rất hay được
          khách hàng yêu cầu xây dựng. Các doanh nghiệp và start-up thường muốn
          hiển thị các cột mốc quan trọng nhất của họ trên trang web.
        </Grid>
        <Grid item xs={4}>
          Nếu bạn muốn trở thành một lập trình viên front end, ví dụ, dự án kiểu
          timeline bằng Javascript có thể là một dự án / tính năng rất hay được
          khách hàng yêu cầu xây dựng. Các doanh nghiệp và start-up thường muốn
          hiển thị các cột mốc quan trọng nhất của họ trên trang web.
        </Grid>
        <Grid item xs={4}>
          Nếu bạn muốn trở thành một lập trình viên front end, ví dụ, dự án kiểu
          timeline bằng Javascript có thể là một dự án / tính năng rất hay được
          khách hàng yêu cầu xây dựng. Các doanh nghiệp và start-up thường muốn
          hiển thị các cột mốc quan trọng nhất của họ trên trang web.
        </Grid> */}
      </Grid>
    </Grid>
  );
}
export default memo(Face);
