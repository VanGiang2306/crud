import { Typography } from "@material-ui/core";
import React, { memo, useState } from "react";
import CustomInput from "./customInput";

function Example(props) {
  const [data, setData] = useState({
    username: "",
    password: "",
  });

  const handleClick = () => {
    setData({ ...data, id: 2 });
  };

  const handleChange = (e) => {
    setData({ ...data, [e.target.name]: e.target.value });
  };
  const handleChangePassword = (e) => {
    setData({ ...data, [e.target.name]: e.target.value });
  };

  return (
    <div>
      <CustomInput
        value={data.username}
        name="username"
        changeVanGiang={handleChange}
      />
      <CustomInput
        value={data.password}
        name="password"
        changeVanGiang={handleChangePassword}
      />
      <Typography>{data.password}</Typography>
      <button onClick={handleClick}>VanGiang</button>
    </div>
  );
}

export default memo(Example);
