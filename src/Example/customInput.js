import { TextField } from "@material-ui/core";
import React, { memo } from "react";

function CustomInput(props) {
  const { value, changeVanGiang, name } = props;
  console.log(1, name, value);
  return (
    <div>
      <TextField
        type="text"
        onChange={changeVanGiang}
        value={value}
        name={name}
      />
    </div>
  );
}

export default memo(CustomInput);
